#!/bin/bash

IMAGE_NAME=docker.io/warzon/clang
TAG=3.9

sudo docker stop $IMAGE_NAME
sudo docker rm -f $IMAGE_NAME &> /dev/null
sudo docker pull $IMAGE_NAME:$TAG
sudo docker run --name clang -i $IMAGE_NAME $@