#!/bin/bash

TAG=5.0
IMAGE_NAME=warzon/clang

docker build -t="$IMAGE_NAME" .
docker tag $IMAGE_NAME docker.io/$IMAGE_NAME:$TAG
docker push docker.io/$IMAGE_NAME:$TAG
