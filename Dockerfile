FROM        ubuntu:16.04
MAINTAINER  Michael Tsukerman <miketsukerman@gmail.com>

ENV         REFRESHED_AT 2016-12-17
ENV         DEBIAN_FRONTEND noninteractive

ADD         llvm.list       /etc/apt/sources.list.d/llvm.list

RUN         dpkg --add-architecture i386 && \
            apt-get update && apt-get upgrade -y && apt-get install -y git wget && \
            wget -O - http://apt.llvm.org/llvm-snapshot.gpg.key|apt-key add - && apt-get update && \
            apt-get install -y --no-install-recommends software-properties-common pkg-config && \
            add-apt-repository -y ppa:george-edison55/cmake-3.x && \
            apt-get install -y libtclap-dev libz-dev && \
            apt-get install -y libboost-filesystem-dev && \
            apt-get install -y --no-install-recommends cmake make patch && \
            apt-get install -y clang-5.0 libclang-common-5.0-dev libclang-5.0-dev libclang1-5.0 libllvm5.0 llvm-5.0 llvm-5.0-dev llvm-5.0-runtime && \
            apt-get -q -y clean && rm -rf /var/cache/apt/archives/* /var/lib/apt/lists/* && \
            locale-gen en_US.UTF-8 && dpkg-reconfigure locales && \
            echo "user:x:1000:0:user,,,:/home:/bin/bash" >> /etc/passwd && echo "user::1000:0:user,,,:/home:/bin/bash" >> /etc/shadow && chmod a+wx /home

ENV         LANG en_US.UTF-8
ENV         LC_ALL en_US.UTF-8
ENV         HOME /home/

CMD         env
